"use strict";

var BreakpointSection = function (section) {
    if(!section) return false;

    var self = this;

    self.section = section;
    self.imagesData = {};
    self.currentSize = "";
    self.config = {
        'sizes': {
            'xs': 1,
            'sm': 576,
            'md': 992,
            'lg': 1280,
            'xl': 1920
        }
    };

    self.init = function () {
        var images = self.section.getElementsByClassName('js-breakpoint-image');
        var counter = 0;

        for (var i = 0; i < images.length; i++) {
            var image = images[i];

            self.imagesData[counter] = {};
            self.imagesData[counter]['node'] = image;
            self.imagesData[counter]['tagName'] = image.tagName;
            self.imagesData[counter]['breakpoints'] = {};

            for (var key in self.config.sizes) {
                var src = image.getAttribute('data-' + key + '-src');
                if (src) self.imagesData[counter]['breakpoints'][key] = src;
            }

            counter += 1;
        }

        // console.log(self.imagesData);
    };

    self.update = function () {
        var size = '';

        for (var key in self.config.sizes) {
            if (self.section.offsetWidth >= self.config.sizes[key]) {
                size = key;
            }
        }

        if (size !== self.currentSize) {
            self.currentSize = size;
            self.render();
        }
    };

    self.render = function () {
        for (var key in self.imagesData) {
            var data = self.imagesData[key];
            var src = '';

            for (var breakpointKey in data.breakpoints) {
                if (breakpointKey === self.currentSize) src = data.breakpoints[breakpointKey];
            }

            if (!src) {
                var _keys = Object.keys(data.breakpoints);
                src = data.breakpoints[Object.keys(data.breakpoints)[_keys.length-1] ];
            }

            if (data.tagName === 'IMG') {
                data.node.src = src;
            } else {
                data.node.style.backgroundImage = 'url(' + src + ')';
            }
        }
    };

    self.init();
    self.update();
};
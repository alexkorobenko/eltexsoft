"use strict";

var breakpointSections = [];

function initIphoneSwiper() {
    var nodes = document.getElementsByClassName("js-iphone-swiper");

    var init = function (el) {
        if (!el.closest(".swiper-section") || !el || el.classList.contains("swiper-container-initialized")) return false;

        var section = el.closest(".swiper-section");
        var slides = section.getElementsByClassName("swiper-slide");
        var bg = section.querySelector(".js-bg");
        var prevEl = section.querySelector(".iphone-swiper__button_prev");
        var nextEl = section.querySelector(".iphone-swiper__button_next");
        var data = {};

        for (var i = 0; i < slides.length; i+=1) {
            if (!data[i]) data[i] = {};
            data[i]["bg"] = slides[i].getAttribute("data-bg");
        }

        var swiper = new Swiper(el, {
            loop: true,
            effect: "fade",
            slidesPerView: 1,
            navigation: {
                prevEl: prevEl,
                nextEl: nextEl
            },
            on: {
                slideChange: function () {
                    if (bg && data[this.realIndex]) bg.style.backgroundColor = data[this.realIndex]["bg"];
                },
                resize: function () {
                    toggleSwiperNav(this);
                }
            }
        });

        toggleSwiperNav(swiper);
    };

    var update = function (el) {
        if (el.offsetWidth > 1) {
            init(el);
        }
    };

    var start = function() {
        for (var i = 0; i < nodes.length; i+=1) {
            update(nodes[i]);
        }
    };

    //
    start();

    window.addEventListener(resizeEventName, debounce(function () {
        start();
    }, 300));
};

function initIpadSwiper() {
    var nodes = document.getElementsByClassName("js-ipad-swiper");

    var init = function (el) {
        if (!el.closest(".swiper-section") || !el || el.classList.contains("swiper-container-initialized")) return false;

        var section = el.closest(".swiper-section");
        var prevEl = section.querySelector(".ipad-swiper__button_prev");

        var swiper = new Swiper(el, {
            loop: true,
            effect: "fade",
            slidesPerView: 1,
            navigation: {
                prevEl: prevEl
            },
            on: {
                resize: function () {
                    toggleSwiperNav(this);
                }
            }
        });

        toggleSwiperNav(swiper);
    };

    var update = function (el) {
        if (el.offsetWidth > 1) {
            init(el);
        }
    };

    var start = function() {
        for (var i = 0; i < nodes.length; i+=1) {
            update(nodes[i]);
        }
    };

    //
    start();

    window.addEventListener(resizeEventName, debounce(function () {
        start();
    }, 300));
};

function initQuotesSwiper() {
    var nodes = document.getElementsByClassName("js-quotes-swiper");

    var init = function (el) {
        var section = el.closest(".swiper-section");
        if (!section) return false;

        var swiper = new Swiper(el, {
            loop: true,
            slidesPerView: "auto",
            //freeMode: true,
            centeredSlides: true,
            spaceBetween: 25,
            on: {
                resize: function () {
                    toggleSwiperNav(this);
                }
            },
            breakpoints: {
                767: {
                    spaceBetween: 15
                }
            }
        });

        toggleSwiperNav(swiper);
        setMaxHeight(section);

        window.addEventListener(resizeEventName, debounce(function () {
            setMaxHeight(section);
        }, 300));
    };

    var setMaxHeight = function (section) {
        var collections = section.getElementsByClassName("swiper-slide");
        var wrapper = section.querySelector(".swiper-wrapper");
        var maxHeight = 0;

        wrapper.style.height = "auto";

        for (var i = 0; i < collections.length; i+=1) {
            if (collections[i].offsetHeight > maxHeight) maxHeight = collections[i].offsetHeight;
        }

        wrapper.style.height = maxHeight + "px";

    };

    for (var i = 0; i < nodes.length; i+=1) {
        init(nodes[i]);
    }
};

function initBreakpointSections() {
    var nodes = document.getElementsByClassName("js-breakpoint-section");

    for (var i = 0; i < nodes.length; i+=1) {
        breakpointSections.push(new BreakpointSection(nodes[i]));
    }
};

function updateBreakpointSections() {
    for (var i = 0; i < breakpointSections.length; i+=1) {
        breakpointSections[i].update();
    }
};

var HeaderMenu = function() {
    var self = this;
    var container = document.querySelector(".header");
    if (!container) return false;

    self.menu = container.querySelector(".js-menu");
    self.navButton = container.querySelector(".js-nav");
    if (!self.menu || !self.navButton) return false;

    self.mask = container.querySelector(".js-mask");

    self.init = function () {
        self.navButton.addEventListener("click", function (e) {
            e.preventDefault();
            self.open();
        }, !1);

        if (self.mask) {
            self.mask.addEventListener("click", function (e) {
                e.preventDefault();
                self.close();
            }, !1);
        }

        window.addEventListener(resizeEventName, function () {
            if (self.navButton.offsetWidth <= 0) {
                self.close();
            }
        });
    };

    self.open = function () {
        document.documentElement.classList.add("header-menu-opened");
        bodyScrollLock.disableBodyScroll(self.menu);
    };

    self.close = function () {
        document.documentElement.classList.remove("header-menu-opened");
        bodyScrollLock.enableBodyScroll(self.menu);
    };

    self.init();

    return self;
};

document.addEventListener("DOMContentLoaded", function () {
    initBreakpointSections();
});

window.onload = function () {
    new HeaderMenu();
    new ScrollWatch();

    initIphoneSwiper();
    initIpadSwiper();
    initQuotesSwiper();

    window.addEventListener(resizeEventName, debounce( function () {
        updateBreakpointSections();
    }, 300));
};
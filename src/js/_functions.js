"use strict";

// проверяем поддержку
if (!Element.prototype.matches) {

    // определяем свойство
    Element.prototype.matches = Element.prototype.matchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector;

}

// проверяем поддержку
if (!Element.prototype.closest) {

    // реализуем
    Element.prototype.closest = function(css) {
        var node = this;

        while (node) {
            if (node.matches(css)) return node;
            else node = node.parentElement;
        }
        return null;
    };
}

var useragents = ['android','astel','audiovox','blackberry','chtml','docomo','ericsson','hand','iphone ','ipod','2me','ava','j-phone','kddi','lg','midp','mini','minimo','mobi','mobile','mobileexplorer','mot-e','motorola','mot-v','netfront','nokia', 'palm','palmos','palmsource','pda','pdxgw','phone','plucker','portable','portalmmm','sagem','samsung','sanyo','sgh','sharp','sie-m','sie-s','smartphone','softbank','sprint','symbian','telit','tsm','vodafone','wap','windowsce','wml','xiino'];
var agt = navigator.userAgent.toLowerCase();
var isMobile = false;
for( var i = 0; i < useragents.length; i++ ){
    if (  agt.indexOf(useragents[i]) != -1 ) {
        isMobile = true;
        break;
    }
}

if (isMobile) {
    document.documentElement.classList.add("is-mobile");
} else {
    document.documentElement.classList.add("is-desktop");
}

var resizeEventName = (isMobile) ? "orientationchange" : "resize";

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function toggleSwiperNav(swiper) {
    var section = swiper.wrapperEl.closest(".swiper-section");
    var action = false;

    if (swiper.params.slidesPerView === "auto") {
        action = swiper.virtualSize > swiper.width + 20;
    } else {
        action = swiper.slides.length > swiper.params.slidesPerView;
    }

    if (action) {
        section.classList.add("swiper-with-nav");
        section.classList.remove("swiper-centered");
    } else {
        section.classList.remove("swiper-with-nav");
        section.classList.add("swiper-centered");
    }
};
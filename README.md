# EltexSoft

> http://eltex-soft.surge.sh/

## Build Setup

``` bash
# install dependencies
npm install

# serve at localhost:3000
gulp
```
// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
// const imagemin = require("gulp-imagemin");
// const newer = require("gulp-newer");
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const browserSync = require('browser-sync').create();
const prettify = require('gulp-html-prettify');
const fileinclude = require('gulp-file-include');
const versionAppend = require('gulp-version-append');


// File paths
const files = {
    htmlPath: 'src/**/*.html',
    scssPath: 'src/scss/**/*.scss',
    jsPath: 'src/js/**/*.js'
};

function htmlTask() {
    return src(['src/*.html'])
        .pipe(fileinclude({
            prefix: '@',
            basepath: '.'
        }))
        .pipe(prettify({indent_char: ' ', indent_size: 4}))
        .pipe(versionAppend(['html', 'js', 'css']))
        .pipe(dest('dist'));
        // .on('end', browserSync.reload);
}

// Sass task: compiles the style.scss file into style.css
function scssTask(){
    return src(files.scssPath)
        .pipe(sourcemaps.init()) // initialize sourcemaps first
        .pipe(sass()) // compile SCSS to CSS
        .pipe(postcss([ autoprefixer(), cssnano() ])) // PostCSS plugins
        .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
        .pipe(dest('dist/css')); // put final CSS in dist folder
        // .on('end', browserSync.reload);
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src([
        files.jsPath
        //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
    ])
        .pipe(concat('bundle.js'))
        .pipe(sourcemaps.init()) // initialize sourcemaps first
        .pipe(uglify())
        .pipe(sourcemaps.write('.')) // write sourcemaps file in current directory
        .pipe(dest('dist/js'));
        // .on('end', browserSync.reload);
}

// Watch task: watch SCSS and JS files for changes
// If any change, run scss and js tasks simultaneously
function watchTask(){
    watch([files.htmlPath, files.scssPath, files.jsPath],
        parallel(htmlTask, scssTask, jsTask));
}

function serve() {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
    //browserSync.watch("dist", browserSync.reload);
}

// Export the default Gulp task so it can be run
// Runs the scss and js tasks simultaneously
// then runs cacheBust, then watch task
exports.default = series(
    htmlTask,
    parallel(scssTask, jsTask),
    parallel(watchTask, serve)
);